import json
import requests

listing_url = 'https://api.coinmarketcap.com/v2/listings/'

request = requests.get(listing_url)
results = request.json()

# print(json.dumps(results, sort_keys=True, indent=4))


data_results = results['data']
for currency in data_results:
    rank = currency['id']
    name = currency['name']
    symbol = currency['symbol']
    print(str(rank) + ': ' + name + ' (' + symbol + ')')


# def data_print():
#     data_results = results['data']
#     for currency in data_results:
#         rank = currency['id']
#         name = currency['name']
#         symbol = currency['symbol']
#         print_data_results = (str(rank) + ': ' + name + ' (' + symbol + ')')
#         #print(print_data_results)

# data_print()

